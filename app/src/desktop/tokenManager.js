const fs = require('fs');
const { isUndefined } = require('util');

//TODO check for existing token file
//TODO Create the file and init them or read it

let tokenFile = { accesstoken: '', refreshtoken: '' };

let modify = false;

const listenerEvents = { accesstoken: 'accesstoken', refreshtoken: 'refreshtoken' };
let listeners = { accesstoken: [], refreshtoken: [] };

const updateFile = () => {
    // console.log('update file');
    // console.log(JSON.stringify(tokenFile));
    fs.writeFile(
        'token',
        JSON.stringify(tokenFile),
        'utf8',
        err => (err ? console.error(err) : null),
    );
};

const on = (event, trigger) => {
    listeners[event].push(trigger);
};

const remove = (event, trigger) => {
    const index = listeners[event].indexOf(trigger);
    if (index >= 0) listeners[event].splice(index, 1);
};

const init = () => {
    const file = fs.readdirSync(process.cwd()).filter(file => file === 'token')[0];
    if (isUndefined(file)) {
        updateFile();
    } else {
        tokenFile = JSON.parse(fs.readFileSync(file, 'utf8'));
    }
    return {
        accessToken,
        refreshToken,
        updateFile,
        listenerEvents,
        on,
        remove,
    };
};

function set(token, from) {
    // console.log('SET: ' + from);
    tokenFile[this.name] = token;
    listeners[this.name].forEach(func => func());
    modify = true;
    setTimeout(() => {
        if (modify) {
            updateFile();
            modify = false;
        }
    }, 2000);
}

function get() {
    return tokenFile[this.name];
}

const accessToken = {
    set: set.bind({ name: 'accesstoken' }),
    get: get.bind({ name: 'accesstoken' }),
};

const refreshToken = {
    set: set.bind({ name: 'refreshtoken' }),
    get: get.bind({ name: 'refreshtoken' }),
};

module.exports = init;
