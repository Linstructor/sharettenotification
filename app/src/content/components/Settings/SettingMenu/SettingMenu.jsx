import React, { Component } from 'react';
import styles from './styles.css';
import { ipcRenderer, remote } from 'electron';

const regexPass = /^[\w\d!$=+()?/]{6,}$/;

export default class SettingsLayout extends Component {
    constructor(props) {
        super(props);
        this.state = { password: '', setcontent: '', show: false };
        this.contentRef = React.createRef();
        this.settings = [
            {
                title: 'Disconnect',
                description: 'Disconnect from your account',
                click: () => {
                    ipcRenderer.send('disconnect');
                    remote.getCurrentWindow().close();
                },
            },
        ];
    }

    handleChange(name, event) {
        event.target.style.borderBottomColor = regexPass.test(event.taget.value)
            ? '#42b983'
            : 'red';
        this.setState({ ...this.state, [name]: event.target.value.toString().trim() });
    }

    showSetting(content) {
        this.setState({ ...this.state, setcontent: content });
        this.setState({ ...this.state, show: true });
    }

    //TODO Change password
    //TODO Revoke token
    //TODO quit when token expired

    render() {
        return (
            <div className={styles.container}>
                {this.settings !== undefined ? (
                    this.settings.map(setting => {
                        return (
                            <div
                                key={setting}
                                className={styles.setting + ' ' + styles.clickable}
                                onClick={() => {
                                    if (setting.content !== undefined) {
                                        //TODO display content
                                    }
                                    if (
                                        setting.click !== undefined &&
                                        typeof setting.click === 'function'
                                    ) {
                                        setting.click();
                                    }
                                }}
                            >
                                <p className={styles.setting_title}>{setting.title}</p>
                                <p className={styles.setting_descr}>{setting.description}</p>
                            </div>
                        );
                    })
                ) : (
                    <p style={{ textAlign: 'center', color: '#BCBDBD' }}>Nothing here</p>
                )}
                <div
                    ref={this.contentRef}
                    className={styles.container}
                    style={{ position: 'absolute', display: this.state.show ? 'block' : 'none' }}
                >
                    {this.state.content}
                </div>
            </div>
        );
    }
}
