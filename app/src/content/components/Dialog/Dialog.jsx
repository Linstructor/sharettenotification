import React, { Component } from 'react';

import styles from './styles.css';

export default class Dialog extends Component {
    render() {
        return (
            <div className={styles.dialog_background}>
                <div className={styles.dialog}>
                    <p className={styles.title}>this.props.title</p>
                    <p className={styles.description}>this.props.description</p>
                </div>
            </div>
        );
    }
}
