import { socketJwt } from '../../middleware/jwt';
import { createFakeToken } from '../common';
import uuid from 'uuid/v4';

export const jwtMiddleware = () => {
    return new Promise((resolve, reject) => {
        const uid = uuid();
        return createFakeToken(process.env.jwt_secret, { cid: uid }).then(token => {
            socketJwt({ authorization: 'Desktop ' + token, cid: uid })
                .then(result => resolve(result))
                .catch(err => reject(err));
        });
    });
};

export const jwtMiddlewareInvalidToken = () => {
    return new Promise((resolve, reject) => {
        socketJwt({
            authorization: 'Desktop soidjfnglkjdsnfgn.qsjhbdfhsbd.qsdihbfqhsbdfjb',
            cid: 'dqfgqkqodfingqje456',
        })
            .then(() => resolve())
            .catch(err => {
                reject(err);
            });
    });
};
