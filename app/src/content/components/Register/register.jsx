import React, { Component } from 'react';
import styles from './styles.css';
import { bindActionCreators } from 'redux';
import * as ActionCreator from '../../../store/main/actionCreators';
import connect from "react-redux/es/connect/connect";

const regexMail = /^[\w\d]+((\.[\w\d]+)?)+@[\w\d]+\.([\w\d]+)((\.[\w\d]+)?)+$/;
const regexPass = /^[\w\d!$=+()?/]{6,}$/;

class register extends Component {
    constructor(props) {
        super(props);
        this.state = { email: '', password: '', vpassword: '', verify: true, invaMsg: '' };
        this.submit = this.submit.bind(this);
        this.action = bindActionCreators(ActionCreator, props.dispatch);
    }

    handleChange(name, event) {
        const checkString = regex => {
            return regex.test(event.target.value) ? '#42b983' : 'red';
        };
        if (name !== 'vpassword') {
            event.target.style.borderBottomColor =
                name === 'email' ? checkString(regexMail) : checkString(regexPass);
        }
        this.setState(
            { ...this.state, [name]: event.target.value.toString().trim() },
            this.verifyPwd,
        );
    }

    verifyPwd() {
        if (this.state.password !== '') {
            if (this.state.password === this.state.vpassword) {
                this.setState({ ...this.state, verify: true });
            } else if (this.state.verify === true) {
                this.setState({ ...this.state, verify: false });
            }
        }
    }

    submit() {
        if (!regexMail.test(this.state.email)) {
            return this.setState({ ...this.state, invaMsg: 'There is a problem with your mail' });
        }
        if (!regexPass.test(this.state.password)) {
            return this.setState({
                ...this.state,
                invaMsg:
                    'Your password must be more than 6 char.\nRestricted specials char !?$=+()/',
            });
        }
        if (this.state.password !== this.state.vpassword) {
            return this.setState({
                ...this.state,
                password: '',
                vpassword: '',
                invaMsg: 'Passwords are not the same',
            });
        }
        fetch('https://localhost:4000/user', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
            }),
        }).then(response => {
            if (response.status === 409) {
                this.setState({ ...this.state, invaMsg: 'Email already used' });
            }
            if (response.status === 400) {
                this.setState({ ...this.state, invaMsg: 'Invalid information' });
            }
            if (response.ok) {
                response.json().then(body => {
                    this.action.setToken(body.accesstoken, 'register');
                    this.action.setRefreshToken(body.refreshtoken, 'register');
                    this.props.history.push('/home');
                });
            }
        });
    }

    render() {
        return (
            <div>
                <div className={styles.input_container}>
                    <p className="material-icons">account_circle</p>
                    <input
                        className={styles.input}
                        placeholder="Email"
                        value={this.state.email}
                        onChange={this.handleChange.bind(this, 'email')}
                        type="email"
                    />
                </div>
                <div className={styles.input_container}>
                    <p className="material-icons">lock</p>
                    <input
                        className={this.state.verify ? '' : styles.prob_input}
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleChange.bind(this, 'password')}
                        type="password"
                    />
                </div>
                <div className={styles.input_container}>
                    <p className="material-icons" style={{ opacity: '0.5' }}>
                        lock
                    </p>
                    <input
                        className={this.state.verify ? '' : styles.prob_input}
                        placeholder="Verify password"
                        value={this.state.vpassword}
                        onChange={this.handleChange.bind(this, 'vpassword')}
                        type="password"
                    />
                </div>
                <div className={styles.button_container}>
                    <div
                        className={styles.register_button}
                        style={{ marginRight: '100px' }}
                        onClick={this.props.history.goBack}
                    >
                        <p
                            className="material-icons"
                            style={{ margin: '11px 10px', fontSize: '23px' }}
                        >
                            arrow_back
                        </p>
                    </div>
                    <div className={styles.register_button} onClick={this.submit}>
                        <p>Register</p>
                    </div>
                </div>
                <p className={styles.invalid}>{this.state.invaMsg}</p>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps)(register);
