import redis from 'redis';
import { promisify, isNull } from 'util';
import Murmurhash from './Murmurhash';

let client, setAsync;

//TODO resolve stream connection error

/**
 * Look if the token exists in the database
 * @param tokenHash
 * @returns {Promise<any>}Error/TokenInfo
 */
const checkRefreshToken = tokenHash => {
    // console.log(tokenHash);
    return new Promise((resolve, reject) => {
        client.get(tokenHash, (err, rtoken) => {
            isNull(err) && !isNull(rtoken) ? resolve(rtoken) : reject('token not found');
        });
    });
};

/**
 * Add a token to the database
 * @param token
 * @param expiration? {Integer}
 * @returns {Promise<any>}Error/Hash
 */
const addRefreshToken = (token, expiration = 600) => {
    return new Promise((resolve, reject) => {
        const refreshToken = { token };
        const hash = Murmurhash(JSON.stringify(refreshToken), 42);
        setAsync(hash, JSON.stringify(refreshToken), 'EX', expiration)
            .then(result => {
                if (result !== 'OK') return reject(result);
                resolve(hash);
            })
            .catch(err => reject(err));
    });
};

/**
 * Remove token from the database
 * @param tokenHash
 * @returns {Promise<any>}Error/(1: yes, 0: no)
 */
const removeRefreshToken = tokenHash => {
    return new Promise((resolve, reject) => {
        client.del(tokenHash, (err, response) => {
            if (err) return reject(err);
            resolve(response);
        });
    });
};

/**
 * Modify refresh token information
 * @param tokenHash
 * @param accesstoken
 * @returns {Promise<any>}
 */
const modifyRefreshTokenInformation = (tokenHash, accesstoken) => {
    return new Promise((resolve, reject) => {
        client.set(
            tokenHash,
            JSON.stringify({ token: accesstoken }),
            'EX',
            600,
            (err, response) => {
                if (err || response !== 'OK') reject('No modification');
                resolve(response);
            },
        );
    });
};

/**
 * Connect to redis
 * @param ip
 * @returns {Promise<any>}Error
 */
export const connect = (ip = '127.0.0.1') => {
    return new Promise((resolve, reject) => {
        try {
            const connection = redis.createClient(`redis://${ip}:${process.env.redis_port}`, {
                retry_strategy: options => {
                    if (options.error && options.error.code === 'ECONNREFUSED') {
                        reject('Redis server redused the connection');
                        return new Error('Redis server redused the connection');
                    }
                    if (options.total_retry_time > 1000 * 15) {
                        reject('Redis connection timeout');
                        return new Error('We have wait too long');
                    }
                    if (options.attempts > 5) {
                        reject('Redis connection max attempt');
                        return undefined;
                    }
                    return 2000;
                },
                password: process.env.redis_pass,
            });
            connection.once('connect', () => {
                client = connection;
                setAsync = promisify(client.set).bind(client);
                resolve();
            });
        } catch (e) {
            reject(e);
        }
    });
};

/**
 * Close connection
 * @returns {Promise<any>}null
 */
export const close = () => {
    return new Promise(async resolve => {
        await client.quit();
        resolve();
    });
};

export const token = {
    addRefreshToken,
    removeRefreshToken,
    checkRefreshToken,
    modifyRefreshTokenInformation,
};
