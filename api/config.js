require('dotenv').load();
import { isUndefined } from 'util';

const quit = message => {
    console.error(message);
    process.exit(1);
};

if (isUndefined(process.env.api_port)) quit('Api port is undefined');
if (isUndefined(process.env.jwt_secret)) quit('Jwt secret is undefined');
if (isUndefined(process.env.jwt_expiration)) quit('Jwt token expiration is undefined');
if (isUndefined(process.env.jwt_refresh_expiration))
    quit('Jwt refresh token expiration is undefined');
if (isUndefined(process.env.bcrypt_saltround)) quit('Bcrypt round number is undefined');
if (isUndefined(process.env.mongo_auth)) quit('Mongo host is undefined');
if (isUndefined(process.env.mongo_pass)) quit('Mongo password is undefined');
if (isUndefined(process.env.mongo_port)) quit('Mongo port is undefined');
if (isUndefined(process.env.redis_auth)) quit('Redis host is undefined');
if (isUndefined(process.env.redis_pass)) quit('Redis password is undefined');
if (isUndefined(process.env.redis_port)) quit('Redis port is undefined');
