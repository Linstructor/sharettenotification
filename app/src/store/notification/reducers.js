import Actions from './actions';
import Default from './default';

/* eslint-disable indent */
export default (state = Default, action) => {
    switch (action.type) {
        case Actions.initNotification:
            return { ...state, ...action.payload };
        default:
            return state;
    }
};
