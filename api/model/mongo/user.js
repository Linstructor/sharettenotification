import mongoose from 'mongoose';

const schema = mongoose.Schema(
    {
        email: { type: String, index: true },
        password: String,
        uid: String,
    },
    { collection: 'users' },
);
mongoose.model('User', schema);
export default mongoose.model('User');
