/*eslint-disable no-undef*/
import config from '../../config';

import * as Redis from '../../databases/redis';
import { createFakeToken } from '../common';
import * as Hash from '../../databases/Murmurhash';

let token, hash, result;

beforeAll(async () => {
    await Redis.connect();
    token = await createFakeToken(process.env.jwt_secret);
    result = { token, expiration: 10 };
    hash = Hash.default(JSON.stringify(result), 42);
});

afterAll(async () => {
    await Redis.close();
});
describe('Token lifecycle', () => {
    test('Creation', () => {
        return expect(Redis.token.addRefreshToken(token, 10)).resolves.not.toBeNull();
    });

    test('Creation with expiration', done => {
        Redis.token.addRefreshToken(token + '7', 2).then(() => {
            setTimeout(() => {
                expect(
                    Redis.token.checkRefreshToken(
                        Hash.default(JSON.stringify({ token: token + '7', expiration: 2 }), 42),
                    ),
                ).rejects.toBe('token not found');
                done();
            }, 4000);
        });
    });

    describe('Check', () => {
        let shash, stoken;

        beforeAll(async () => {
            stoken = await createFakeToken(process.env.jwt_secret);
            shash = Hash.default(JSON.stringify({ token: stoken, expiration: 15 }), 42);
            await Redis.token.addRefreshToken(stoken, 15);
        });

        test('Success', () => {
            return Redis.token.checkRefreshToken(shash).then(response => {
                return expect(response).toBe(JSON.stringify({ token: stoken, expiration: 15 }));
            });
        });
        test('Fail not found', () => {
            return expect(Redis.token.checkRefreshToken(hash + 'b')).rejects.toBe(
                'token not found',
            );
        });
    });

    describe('Remove', () => {
        test('Success', () => {
            return expect(Redis.token.removeRefreshToken(hash)).resolves.toBe(1);
        });

        test('Fail', () => {
            return expect(Redis.token.removeRefreshToken(hash)).resolves.toBe(0);
        });
    });
});
