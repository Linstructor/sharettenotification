import React, { Component } from 'react';
import style from './style.css';

export default class Progressbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            infinite: props.infinite,
            value: props.infinite ? 100 : props.value,
            round: props.round,
            size: props.size,
            class: props.infinite ? style.infinite_progress : style.progress,
        };
    }

    render() {
        return this.state.round ? (
            <div
                style={{
                    width: this.state.size + 'px',
                    height: this.state.size + 'px',
                    margin: 'auto',
                    marginTop: '7px',
                    position: 'relative',
                }}
            >
                <div className={style.infinite_round_progress} />
            </div>
        ) : (
            <div className={style.progress_container}>
                <div className={this.state.class} style={{ width: this.state.value + '%' }} />
            </div>
        );
    }
}
