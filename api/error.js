let error;
const devError = {
    TOKEN_INVALID: 'Invalid token',
    IP_NOT_MATCH: 'Ip not match with token',
    NO_TOKEN: 'No token in request',
    PARTIAL_INVALID: 'Information missing',
    PLATFORM_NOT_MATCH: 'Platform not match with token',
    PROTOCOL_INVALID: 'Invalid protocol',
    TOKEN_NOT_FOUND: "Token wasn't in Redis",
    TOKEN_NOT_MATCH: 'Token information doesnt match with Redis information',
    TOKEN_EXP: 'Token expired',
};

const prodError = {
    TOKEN_INVALID: 'Error 0001',
    IP_NOT_MATCH: 'Error 0002',
    NO_TOKEN: 'Error 0003',
    PARTIAL_INVALID: 'Error 0004',
    PLATFORM_NOT_MATCH: 'Error 0005',
    PROTOCOL_INVALID: 'Error 0006',
    TOKEN_NOT_FOUND: 'Error 0007',
    TOKEN_NOT_MATCH: 'Error 0008',
    TOKEN_EXP: 'Error 0009',
};

error = process.env.state !== 'development' ? prodError : devError;
export default error;
