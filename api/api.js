import { isUndefined, isNull } from 'util';

import https from 'https';
import express from 'express';
import parser from 'body-parser';
import sanitize from 'sanitize';

import * as jwt from './middleware/jwt';

import * as WsocketManager from './socket/manager';

import * as Mongodb from './databases/mongodb';
import * as Redis from './databases/redis';

import fs from 'fs';

let server;
const app = express();
app.use(parser.json());
app.use(sanitize.middleware);
app.use(jwt.expressJwt(['/login', '/token', { path: '/user', method: 'post' }]));

app.get('/auth', (req, res) => {
    res.sendStatus(200);
});

app.post('/token', (req, res) => {
    if (req.queryString('refresh') !== 'true') return res.sendStatus(404);
    if (isUndefined(req.bodyString('accesstoken')) || isUndefined(req.bodyString('refreshtoken')))
        return res.sendStatus(400);
    jwt.refreshToken(req.bodyString('accesstoken'), req.bodyString('refreshtoken'))
        .then(result => {
            return res.status(201).json(result);
        })
        .catch(err => {
            return res.sendStatus(410);
        });
});

app.post('/user', (req, res) => {
    if (
        isNull(req.bodyEmail('email')) ||
        isUndefined(req.bodyEmail('email')) ||
        isNull(req.bodyString('password')) ||
        isUndefined(req.bodyString('password'))
    )
        return res.sendStatus(400);

    Mongodb.user
        .createUser(req.bodyEmail('email'), req.bodyString('password'))
        .then(uid => {
            jwt.createToken(uid, req.bodyEmail('email'))
                .then(token => {
                    jwt.createRefreshToken(token)
                        .then(refreshToken => {
                            return res
                                .status(201)
                                .json({ accesstoken: token, refreshtoken: refreshToken });
                        })
                        .catch(() => {
                            return res.sendStatus(503);
                        });
                })
                .catch(() => {
                    return res.sendStatus(503);
                });
        })
        .catch(() => {
            return res.sendStatus(409);
        });
});

app.patch('/user', (req, res) => {
    if (isNull(req.bodyString('password')) || isUndefined(req.bodyString('password'))) {
        return res.sendStatus(400);
    }
    Mongodb.user
        .modifyPassword(jwt.getTokenInformation(req.token).uid, req.bodyString('password'))
        .then(() => {
            return res.sendStatus(204);
        })
        .catch(err => {
            return res.sendStatus(503);
        });
});

app.post('/login', (req, res) => {
    if (
        isNull(req.bodyEmail('email')) ||
        isNull(req.bodyString('password')) ||
        isUndefined(req.bodyEmail('email')) ||
        isUndefined(req.bodyString('password'))
    ) {
        return res.sendStatus(400);
    }

    Mongodb.user
        .verifyUserCredentials(req.bodyEmail('email'), req.bodyString('password'))
        .then(uid => {
            jwt.createToken(uid, req.bodyEmail('email'))
                .then(token => {
                    jwt.createRefreshToken(token)
                        .then(refreshToken => {
                            return res
                                .status(200)
                                .json({ accesstoken: token, refreshtoken: refreshToken });
                        })
                        .catch(() => {
                            return res.sendStatus(503);
                        });
                })
                .catch(() => {
                    return res.sendStatus(503);
                });
        })
        .catch(() => {
            return res.sendStatus(403);
        });
});

app.get('*', (req, res) => {
    res.status(404).send('Not found ' + req.url + ' ' + req.method);
});
app.post('*', (req, res) => {
    res.status(404).send('Not found ' + req.url + ' ' + req.method);
});

export const start = () => {
    return new Promise((resolve, reject) => {
        Promise.all([
            Redis.connect('127.0.0.1'),
            Mongodb.connect(
                '127.0.0.1',
                'sharette',
            ),
        ])
            .then(() => {
                const options = {
                    key: fs.readFileSync(__dirname + '/certificate/server.key'),
                    cert: fs.readFileSync(__dirname + '/certificate/server.crt'),
                    passphrase: 'password',
                };

                server = https.createServer(options, app).listen(process.env.api_port, () => {
                    console.log('Server listening localhost: ' + process.env.api_port);
                    WsocketManager.open();
                });
            })
            .catch(err => {
                console.error('Error during databases connection: ' + err);
                reject();
            });
    });
};

export const close = () => {
    return new Promise(async resolve => {
        try {
            await server.close();
            await Mongodb.close();
            await Redis.close();
            console.warn('Api stopped');
        } catch (e) {
            console.error('Error during stopping: ' + e);
        }
        resolve();
    });
};
