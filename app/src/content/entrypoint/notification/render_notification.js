import React from 'react';
import { render } from 'react-dom';
import { ipcRenderer } from 'electron';

import { bindActionCreators, createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from '../../../store/notification/reducers';

import './style.css';

import Notification from '../../components/Notification/Notification.jsx';
import * as ActionCreator from '../../../store/notification/actionCreators';

const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
const actions = bindActionCreators(ActionCreator, store.dispatch);

render(
    <Provider store={store}>
        <Notification />
    </Provider>,
    document.getElementById('app'),
);

ipcRenderer.on('content', (_, data) => {
    actions.initNotification(data);
});

if (module.hot) {
    module.hot.accept();
}
