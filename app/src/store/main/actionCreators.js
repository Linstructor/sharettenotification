import Actions from './actions';

export const setToken = (token, from) => {
    return {
        type: Actions.tokenSetter,
        payload: { accesstoken: token, from, goMain: true },
    };
};

export const setTokenWithoutCallback = (token, from) => {
    return {
        type: Actions.tokenSetter,
        payload: { accesstoken: token, from, goMain: false },
    };
};

export const setRefreshToken = (refreshToken, from) => {
    return {
        type: Actions.refreshTokenSetter,
        payload: { refreshToken, from },
    };
};

export const setStartToken = (token, refreshToken, from) => {
    return {
        type: Actions.defaultSetter,
        payload: { accesstoken: token, refreshToken, from },
    };
};
