import React, { Component } from 'react';
import styles from './style.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreator from '../../../store/main/actionCreators';

import Progessbar from '../ProgressBar/Progressbar';

import { ipcRenderer } from 'electron';
import Snackbar from '../Snackbar/Snackbar';

console.log('/LoaderPage');

class Loaderpage extends Component {
    constructor(props) {
        super(props);
        this.action = bindActionCreators(ActionCreator, props.dispatch);
        ipcRenderer.on('content', () => {
            this.testAuth();
        });
        this.state = { error: false };
    }

    render() {
        return (
            <div>
                <div className={styles.progess_bar}>
                    <Progessbar value={50} infinite />
                    {this.state.error ? (
                        <Snackbar msg="The server seems to be down. Retry in several minutes" />
                    ) : null}
                </div>
            </div>
        );
    }

    componentDidMount() {
        ipcRenderer.send('askToken');
        console.log('Token asked');
    }

    testAuth() {
        setTimeout(() => {
            const store = this.props.store;
            const push = this.props.history.push;
            if (store.accesstoken === '') {
                push('/login');
            } else {
                fetch('https://localhost:4000/auth', {
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + store.accesstoken,
                    },
                    rejectUnauthorized: false,
                })
                    .then(response => {
                        if (response.status === 200) {
                            console.log('token is ok');
                            ipcRenderer.send('startSocket');
                            return push('/home');
                        }
                        if (response.status !== 200) {
                            if (response.status === 410) {
                                console.log('token expired');
                                if (store.refreshToken !== '') {
                                    fetch('https://localhost:4000/token?refresh=true', {
                                        method: 'POST',
                                        headers: {
                                            Accept: 'application/json',
                                            'Content-Type': 'application/json',
                                        },
                                        body: JSON.stringify({
                                            accesstoken: store.accesstoken,
                                            refreshtoken: store.refreshToken,
                                        }),
                                    })
                                        .then(result => {
                                            if (result.status === 201) {
                                                return result.json().then(result => {
                                                    this.action.setToken(
                                                        result.accesstoken,
                                                        'loaderpage refresh token',
                                                    );
                                                    this.action.setRefreshToken(
                                                        result.refreshtoken,
                                                        'loaderpage refresh token',
                                                    );
                                                    console.log('Token refreshed');
                                                    ipcRenderer.send('startSocket');
                                                    return push('/home');
                                                });
                                            }
                                            console.log('refresh token expired');
                                            return push('/login');
                                        })
                                        .catch(() => push('/login'));
                                }
                            }
                            return push('/login');
                        } else {
                            ipcRenderer.send('startSocket');
                            push('/home');
                        }
                    })
                    .catch(err => {
                        console.log('fetch error');
                        this.setState({ ...this.state, error: true });
                    });
            }
        }, 2000);
    }
}

const mapStateToProps = state => {
    return {
        store: {
            accesstoken: state.accesstoken,
            refreshToken: state.refreshToken,
        },
    };
};

export default connect(mapStateToProps)(Loaderpage);
