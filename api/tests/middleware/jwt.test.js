/* eslint-disable no-undef */
import config from '../../config';
import error from '../../error';
import { jwtMiddleware, jwtMiddlewareInvalidToken } from './jwt';

test('Middleware JWT should accept', () => {
    return expect(jwtMiddleware()).resolves.toBe('Ok');
});

test('Middleware JWT should reject for invalid token', () => {
    return expect(jwtMiddlewareInvalidToken()).rejects.toBe(error.TOKEN_INVALID);
});
