import { createFakeToken } from '../common';
import request from 'supertest';
import error from '../../error';

export const GetAuth = async app => {
    return new Promise(async (resolve, reject) => {
        try {
            const token =
                'Bearer ' + (await createFakeToken(process.env.jwt_secret, { platform: 'Bearer' }));
            request(app)
                .get('/auth')
                .set('Authorization', token)
                .expect(200)
                .end((err, res) => {
                    err ? reject(err.message + ':\t' + res.error.text) : resolve();
                });
        } catch (err) {
            reject();
        }
    });
};

export const GetAuthFailed = async app => {
    return new Promise(async (resolve, reject) => {
        try {
            const token = 'Bearer ' + (await createFakeToken('falsesecret'));
            request(app)
                .get('/auth')
                .set('Authorization', token)
                .expect(401)
                .end((err, res) => {
                    if (err) reject(err.message + ':\t' + res.error.text);
                    res.error.text !== error.TOKEN_INVALID
                        ? reject('Request return a different error:\t' + res.error.text)
                        : resolve();
                });
        } catch (err) {
            reject(err);
        }
    });
};
