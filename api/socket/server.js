const WebSocket = require('ws');
const error = require('../error');
const jwt = require('../middleware/jwt');
const { isUndefined, isNull } = require('util');
const https = require('https');
const fs = require('fs');
const Redis = require('../databases/redis');

const options = {
    key: fs.readFileSync('./api/certificate/server.key'),
    cert: fs.readFileSync('./api/certificate/server.crt'),
    passphrase: 'password',
};

let wsServer;
let httpServer;

let interval;

//TODO stop main process when socket server has error

process.on('message', async msg => {
    if (msg.action === 'close') {
        clearInterval(interval);
        await httpServer.close();
        await wsServer.close();
        await process.send({ state: 'closed' });
        process.exit(0);
    }
});

const Hotel = {
    rooms: {},
    addSocket: socket => {
        if (isUndefined(Hotel.rooms[socket.uid])) return (Hotel.rooms[socket.uid] = [socket]);
        Hotel.rooms[socket.uid].push(socket);
    },
    removeSocket: socket => {
        if (!isUndefined(Hotel.rooms[socket.uid])) {
            const index = Hotel.rooms[socket.uid].indexOf(
                Hotel.rooms[socket.uid].filter(isocket => isocket.id === socket.id)[0],
            );
            Hotel.rooms[socket.uid].splice(index, 1);
            if (Hotel.rooms[socket.uid].length === 0) delete Hotel.rooms[socket.uid];
        }
    },
    inform: (source, msg, platform) => {
        Hotel.rooms[source.uid]
            .filter(socket => socket.id !== source.id && socket.platform === platform)
            .forEach(socket => {
                socket.send(JSON.stringify(msg));
            });
    },
};

Redis.connect().then(() => {
    httpServer = https.createServer(options).listen(4001, () => {
        wsServer = new WebSocket.Server(
            {
                server: httpServer,
                verifyClient: (info, cb) => {
                    return cb(true);
                    if (
                        isNull(info.req.headers.authorization) ||
                        isUndefined(info.req.headers.authorization)
                    )
                        return cb(false, 400, error.NO_TOKEN);

                    const bearer = info.req.headers.authorization.split(' ')[0];
                    if (bearer !== 'Desktop' || bearer !== 'Desktop')
                        return cb(false, 401, 'Bad bearer');

                    jwt.socketJwt(info.req.headers)
                        .then(() => {
                            cb(true);
                        })
                        .catch(err => {
                            console.log('token: ' + err);
                            cb(false, 401, err.toString());
                        });
                },
            },
            err => {
                if (err) {
                    console.log(err);
                    process.exit(1);
                }
            },
        );

        wsServer.on('connection', (ws, req) => {
            //TODO regroup user
            ws.id =
                '_' +
                Math.random()
                    .toString(36)
                    .substr(2, 9);
            ws.uid = jwt.getTokenInformation(req.headers.authorization.split(' ')[1]).uid;
            ws.platform = req.headers.authorization.split(' ')[0];
            Hotel.addSocket(ws);

            // console.log(Object.keys(Hotel.rooms).map(key => key + ' ' + Hotel.rooms[key].length));

            ws.on('error', () => {
                ws.terminate();
            });

            ws.on('message', msg => {
                msg = JSON.parse(msg);

                switch (msg.action) {
                    case 'notification':
                        return share(msg, ws, 'Desktop');
                    case 'refresh':
                        return refresh(msg, ws);
                    case 'verify':
                        return verify(msg, ws);
                    case 'response':
                        return share(msg, ws, 'Mobile');
                }
            });

            ws.on('close', () => {
                Hotel.removeSocket(ws);
                console.log(Object.keys(Hotel.rooms).map(key => key + ' ' + Hotel.rooms[key].length));
            });
        });

        interval = setInterval(() => {
            // if (wsServer.clients.length > 0) console.log('Test clients');
            wsServer.clients.forEach(ws => {
                if (ws.isAlive === false) return ws.close(4001);

                ws.isAlive = false;
                ws.send(JSON.stringify({ action: 'paper' }));
            });
        }, 30000);

        const refresh = (msg, ws) => {
            // console.log(msg.data);
            if (isNull(msg.data.accesstoken) || isNull(msg.data.refreshtoken))
                return ws.close(4001);

            jwt.refreshToken(msg.data.accesstoken, msg.data.refreshtoken)
                .then(accesstoken => {
                    ws.send(JSON.stringify({ action: 'newToken', data: accesstoken }));
                    ws.isOk = true;
                })
                .catch(err => {
                    ws.close(4001);
                });
        };

        const verify = (msg, ws) => {
            ws.isAlive = true;

            try {
                jwt.checktoken(msg.data.accesstoken);
                ws.isAlive = true;
            } catch (e) {
                if (e === error.TOKEN_INVALID) return ws.close(4001);

                ws.isOk = false;
                ws.send(JSON.stringify({ action: 'expire' }));

                setTimeout(() => {
                    if (!ws.isOk) {
                        console.log('Token not refreshed');
                        return ws.close(4001);
                    }
                }, 10000);
            }
        };

        const share = (msg, ws, platform) => {
            Hotel.inform(ws, msg, platform);
        };
    });
});
