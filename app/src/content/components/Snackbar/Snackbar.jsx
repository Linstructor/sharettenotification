import React, { Component } from 'react';
import style from './styles.css';

export default class Snackbar extends Component {
    render() {
        return (
            <div className={style.snackbar}>
                <p
                    style={{
                        color: this.props.color ? this.props.color : 'white',
                        textAlign: 'center',
                        marginTop: '15px',
                        fontSize: '13px',
                        fontWeight: 900,
                    }}
                >
                    {this.props.msg}
                </p>
            </div>
        );
    }
}
