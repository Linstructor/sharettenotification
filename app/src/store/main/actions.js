export default {
    tokenSetter: 'SET_TOKEN',
    refreshTokenSetter: 'SET_REFRESH_TOKEN',
    defaultSetter: 'SET_DEFAULT_TOKEN',
};
