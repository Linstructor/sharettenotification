import React, { Component } from 'react';
import styles from './style.css';
import PropTypes from 'prop-types';
import { ipcRenderer, remote } from 'electron';
import connect from 'react-redux/es/connect/connect';

const closeTO = setTimeout(
    () => /*remote.getCurrentWindow().close()*/ console.log('Seems ok'),
    7000,
);

class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = { response: '' };
        this.enterPressed = this.enterPressed.bind(this);
        this.sendButton = React.createRef();
        this.responseInput = React.createRef();
    }

    sendResponse() {
        ipcRenderer.send('notifRespond', {
            response: this.state.response,
            notifId: this.props.store.notifId,
        });
        remote.getCurrentWindow().close();
    }

    handleResponse(event) {
        if (event.target.value.length === 1) {
            this.setState({ ...this.state, response: event.target.value.toUpperCase() });
        } else {
            this.setState({ ...this.state, response: event.target.value });
        }
    }

    enterPressed(event) {
        if (event.key === 'Enter') {
            if (this.state.response !== '') this.sendButton.current.click();
        }
    }

    componentDidMount() {
        ipcRenderer.send('notifContent', remote.getCurrentWindow().id);
    }

    onHover() {
        clearTimeout(closeTO);
        this.responseInput.current.focus();
    }

    render() {
        return (
            <div className={styles.notif} onMouseOver={this.onHover.bind(this)} onKeyPress={this.enterPressed}>
                <a
                    className={styles.close_button + ' material-icons'}
                    onClick={() => {
                        const window = remote.getCurrentWindow();
                        window.close();
                    }}
                >
                    close
                </a>
                <div className={styles.flex_row}>
                    {this.props.store.hasImage ? <img className={styles.img_notif} /> : null}
                    <div className={styles.notif_text}>
                        <p className={styles.notif_text_title}>{this.props.store.title}</p>
                        <p className={styles.notif_text_content}>{this.props.store.description}</p>
                    </div>
                </div>
                {this.props.store.isReplyable ? (
                    <div className={styles.input_container}>
                        <div className={styles.flex_row}>
                            <input
                                type="text"
                                className={styles.input_notif}
                                placeholder="Respond"
                                onChange={this.handleResponse.bind(this)}
                                ref={this.responseInput}
                                value={this.state.response}
                            />
                            <a
                                className={styles.send_button + ' ' + 'material-icons'}
                                onClick={this.sendResponse.bind(this)}
                                ref={this.sendButton}
                            >
                                send
                            </a>
                        </div>
                    </div>
                ) : null}
            </div>
        );
    }
}

Notification.propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
    hasImage: PropTypes.bool,
    isReplyable: PropTypes.bool,
};

const mapStateToProps = state => {
    return {
        store: {
            title: state.title,
            description: state.description,
            hasImage: state.hasImage,
            isReplyable: state.isReplyable,
            notifId: state.notifId,
        },
    };
};

export default connect(mapStateToProps)(Notification);
