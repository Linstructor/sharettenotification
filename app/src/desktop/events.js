const events = {
    showNotification: 'showNotif',
    showMain: 'showMain',
    startSocket: 'startSocket',
};

module.exports = events;
