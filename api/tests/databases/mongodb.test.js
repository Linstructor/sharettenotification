/* eslint-disable no-undef */
import config from '../../config';
import * as mongodb from '../../databases/mongodb';

const fakeUser = { email: 'test.email@mail.com', password: 'TestPassword' };

beforeAll(async () => {
    await mongodb.connect();
});

afterAll(() => {
    mongodb.user
        .deleteWithInfo({ email: fakeUser.email })
        .then(() => mongodb.close())
        .catch(() => mongodb.close());
});

test('Create user in mongo', () => {
    return mongodb.user
        .createUser(fakeUser.email, fakeUser.password)
        .then(uid => {
            return mongodb.user
                .getUserInformation({ uid })
                .then(user => expect(user).not.toBeNull());
        })
        .catch(err => console.error(err));
});

describe('Get user information', () => {
    test('With email', () => {
        return expect(
            mongodb.user.getUserInformation({ email: fakeUser.email }),
        ).resolves.not.toBeNull();
    });

    test('With uid', () => {
        return expect(
            mongodb.user.getUserInformation({ email: fakeUser.email }),
        ).resolves.not.toBeNull();
    });

    test('Without vital information', () => {
        return expect(mongodb.user.getUserInformation({})).rejects.toBe('No info to find the user');
    });

    test('Not present in db', () => {
        return expect(
            mongodb.user.getUserInformation({ email: fakeUser.email + 'a' }),
        ).rejects.toBe('No user');
    });
});

describe('Verify user credentials', () => {
    test('Accept', () => {
        return expect(
            mongodb.user.verifyUserCredentials(fakeUser.email, fakeUser.password),
        ).resolves.not.toBeNull();
    });

    test('Refuse', () => {
        return expect(
            mongodb.user.verifyUserCredentials(fakeUser.email, 'badpassword'),
        ).rejects.toBe('Denied');
    });
});
