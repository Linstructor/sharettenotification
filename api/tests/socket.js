import { clientVerification } from '../socket/server';
import { createFakeToken } from './common';

export const socketClientSuccess = () => {
    return new Promise((resolve, reject) => {
        createFakeToken(process.env.jwt_secret, { platform: 'Desktop' })
            .then(token => {
                clientVerification({
                    httpRequest: {
                        headers: {
                            authorization: 'Desktop ' + token,
                            'sec-websocket-protocol': 'sharette-protocol',
                        },
                        socket: {
                            _peername: {
                                address: '::ffff:127.0.0.1',
                            },
                        },
                    },
                })
                    .then(() => resolve())
                    .catch(err => reject(err));
            })
            .catch(err => reject(err));
    });
};

export const socketClientFail = () => {
    return new Promise((resolve, reject) => {
        createFakeToken(process.env.jwt_secret, { platform: 'Mobile' })
            .then(token => {
                clientVerification({
                    httpRequest: {
                        headers: {
                            authorization: 'Mobile ' + token,
                            'sec-websocket-protocol': 'sharette-protocol',
                        },
                        socket: {
                            _peername: {
                                address: '127.0.0.1',
                            },
                        },
                    },
                })
                    .then(() => reject('Should not be ok'))
                    .catch(() => resolve());
            })
            .catch(err => reject(err));
    });
};
