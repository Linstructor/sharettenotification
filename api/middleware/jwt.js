import jwt from 'jsonwebtoken';
import error from '../error';
import { isUndefined, isArray } from 'util';
import * as Redis from '../databases/redis';

let ignoredPaths = {};

/**
 * Verify access token
 * @param requestInfos
 * @returns {Promise<any>}Error/'Ok'
 */
const tokenVerification = requestInfos => {
    return new Promise((resolve, reject) => {
        if (isUndefined(requestInfos.authorization)) return reject(error.NO_TOKEN);
        const split = requestInfos.authorization.split(' ');
        if (split.length !== 2) return reject(error.NO_TOKEN);
        requestInfos['bearer'] = split[0];
        requestInfos['token'] = split[1];

        try {
            checktoken(requestInfos.token);
            resolve('Ok');
        } catch (e) {
            reject(e);
        }
    });
};

/**
 * Express middleware
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
const middleware = (req, res, next) => {
    if (Object.keys(ignoredPaths).includes(req.path)) {
        if (isArray(ignoredPaths[req.path])) {
            if (ignoredPaths[req.path].includes(req.method)) return next();
        } else {
            if (ignoredPaths[req.path] === '*' || req.method === ignoredPaths[req.path])
                return next();
        }
    }

    const authorization = req.headerString('authorization');
    tokenVerification({
        authorization,
    })
        .then(() => {
            req['token'] = authorization.toString().split(' ')[1];
            next();
        })
        .catch(err => {
            if (err === error.TOKEN_EXP) return res.sendStatus(410);
            if (err === error.NO_TOKEN) return res.sendStatus(400);
            return res.sendStatus(401);
        });
};

/**
 * Create access token
 * @param uid
 * @returns {Promise<any>}Error/token
 */
export const createToken = (uid, email) => {
    return new Promise((resolve, reject) => {
        if (isUndefined(uid)) return reject('Undefined value');
        jwt.sign(
            { uid, email },
            process.env.jwt_secret,
            { expiresIn: parseInt(process.env.jwt_expiration) },
            (err, token) => {
                if (err) return reject(err);
                resolve(token);
            },
        );
    });
};

/**
 * Create refresh token
 * @param token
 * @returns {Promise<any>}Error/refreshtoken
 */
export const createRefreshToken = token => {
    return new Promise((resolve, reject) => {
        Redis.token
            .addRefreshToken(token)
            .then(refreshTk => resolve(refreshTk))
            .catch(err => reject(err));
    });
};

/**
 * Refresh access token
 * @param token
 * @param refreshToken
 * @returns {Promise<any>}Error/{Object}Accesstoken
 */
export const refreshToken = (token, refreshToken) => {
    return new Promise((resolve, reject) => {
        Redis.token
            .checkRefreshToken(refreshToken)
            .then(result => {
                //TODO modify refresh token info to correspond with new accesstoken
                if (JSON.parse(result).token !== token) return reject('Wrong refresh token');
                const decoded = jwt.decode(token);
                createToken(decoded.uid, decoded.email)
                    .then(token => {
                        Redis.token
                            .modifyRefreshTokenInformation(refreshToken, token)
                            .then(() => {
                                resolve({ accesstoken: token });
                            })
                            .catch(err => {
                                reject(err);
                            });
                    })
                    .catch(() => {
                        reject(error.TOKEN_INVALID);
                    });
            })
            .catch(err => {
                if (err === 'token not found') return reject('Refresh token expired');
                reject(err);
            });
    });
};

/**
 * Decode token and return information
 * @param token
 * @returns {*}
 */
export const getTokenInformation = token => {
    return jwt.decode(token);
};

export const checktoken = token => {
    jwt.verify(token, process.env.jwt_secret, (err, decodedToken) => {
        if (err || !decodedToken) {
            if (err.name === 'TokenExpiredError') throw error.TOKEN_EXP;
            throw error.TOKEN_INVALID;
        }
    });
};

/**
 * Create and init express jwt middleware
 * @param paths
 * @returns Express middleware
 */
export const expressJwt = (paths = []) => {
    paths.forEach(path => {
        if (typeof path === 'string') ignoredPaths[path] = '*';
        if (typeof path === 'object') {
            ignoredPaths[path.path] = isArray(path.method)
                ? path.method.map(method => method.toString().toUpperCase())
                : path.method.toString().toUpperCase();
        }
    });
    return middleware;
};

export const socketJwt = tokenVerification;
