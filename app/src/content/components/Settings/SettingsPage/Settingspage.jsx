import React, { Component } from 'react';
import SettingsMenu from '../SettingMenu/SettingMenu';
import styles from './styles.css';
import Settingcontent from "../SettingContent/Settingcontent";

export default class Settingspage extends Component {
    render() {
        return (
            <div className={styles.full}>
                <AppBar
                    title="Settings"
                    leftIcon="arrow_back_ios"
                    onLeftIconClick={null}
                />
                <div className={styles.wrapper}>
                    <div className={styles.menu}>
                        <SettingsMenu />
                    </div>
                    <div className={styles.content}>
                        <Settingcontent content="jpp" />
                    </div>
                </div>
            </div>
        );
    }
}
