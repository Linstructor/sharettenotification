const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    watch: true,
    target: 'electron-main',
    entry: {
        home: './app/src/content/entrypoint/home/render_home.js',
        notificationPanel: './app/src/content/entrypoint/notification/render_notification.js',
    },
    output: {
        path: __dirname + '/app/build',
        publicPath: 'build/',
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ['react'],
                    plugins: ['transform-object-rest-spread'],
                },
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    loader: 'css-loader',
                    options: {
                        modules: true,
                    },
                }),
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[ext]?[hash]',
                },
            },
        ],
    },

    plugins: [
        new ExtractTextPlugin({
            filename: '[name].css',
            disable: false,
            allChunks: true,
        }),
    ],

    resolve: {
        extensions: ['.js', '.json', '.jsx'],
    },
};
