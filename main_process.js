const electron = require('electron');
const { app, BrowserWindow, ipcMain } = electron;

const tokenManager = require('./app/src/desktop/tokenManager')();

const path = require('path');

const events = require('./app/src/desktop/events');

const socket = require('./app/src/desktop/socket');

require('electron-reload')(__dirname + '/app', {
    electron: path.join(__dirname, 'node_modules', '.bin', 'electron.cmd'),
});

let screen = null,
    tray,
    notifs = {},
    queue = [];

//TODO resolve problem where sending undefined refresh key

const notificationOptions = {
    width: 400,
    // resizable: false,
    autoHideMenuBar: true,
    frame: false,
    alwaysOnTop: true,
    skipTaskbar: true,
    show: false,
    maximizable: false,
    minimizable: false,
    transparent: true,
    fullscreenable: false,
    vibrancy: 'ultra-dark',
};

const createWindow = (path, winOptions) => {
    let window = new BrowserWindow(winOptions);
    window.loadURL(`file://${__dirname}/app/src/content/entrypoint/${path}/index.html`);

    window.webContents.session.clearCache(err => {
        if (err) console.error(err);
    });

    window.on('ready-to-show', () => {
        window.show();
    });

    if (path === 'notification') {
        window.on('close', () => {
            console.log('notification closed');
            if (notifs[window.id] !== 'undefined') delete notifs[window.id];
            if (queue.length > 0) {
                setTimeout(() => {
                    const notif = queue.shift();
                    const notifWindow = createWindow('notification', notif.notifOpts);
                    notifs[notifWindow.id] = notif.content;
                }, 1000);
            }
        });
    }
    return window;
};

const createMainWindow = () => {
    const mainWindow = createWindow(
        'home',
        {
            title: 'Sharette Manager',
            resizable: false,
            width: 425,
            height: 700,
            minWidth: 425,
            minHeight: 700,
            show: false,
            autoHideMenuBar: true,
        },
        {
            accesstoken: tokenManager.accessToken.get(),
            refreshtoken: tokenManager.refreshToken.get(),
        },
    );
    const informNewToken = () => {
        console.log('Send new access token');
        mainWindow.webContents.send('newTokenWClb', tokenManager.accessToken.get());
    };

    tokenManager.on(tokenManager.listenerEvents.accesstoken, informNewToken);

    mainWindow.on('close', () => {
        tokenManager.remove(tokenManager.listenerEvents.accesstoken, informNewToken);
    });
};

ipcMain.on('disconnect', () => {
    socket.close();
    tokenManager.accessToken.set('', 'Main process ipc event disconnect');
    tokenManager.refreshToken.set('', 'Main process ipc event disconnect');
    app.emit(events.showMain);
});

ipcMain.on('notifRespond', (event, response) => {
    //TODO finish it
    console.log('Notification closed with response');
    console.log(response);
    socket.respondNotif(response);
});

ipcMain.on('notifContent', (event, id) => {
    if (notifs[id] !== 'undefined') event.sender.webContents.send('content', notifs[id]);
});

ipcMain.on('askToken', event => {
    event.sender.webContents.send('content', {
        accesstoken: tokenManager.accessToken.get(),
        refreshtoken: tokenManager.refreshToken.get(),
    });
});

ipcMain.on(events.startSocket, event => {
    if (!socket.is(socket.status.connected)) {
        console.log('Start socket');
        socket
            .connect(tokenManager)
            .then(() => {
                console.log('Socket connected');
                socket.listen(tray);
            })
            .catch(err => {
                console.error('Error during websocket connection: ' + err);
            });
    }
});

ipcMain.on('newRefreshToken', (event, data) => {
    tokenManager.refreshToken.set(data.refreshToken, 'Main process ipc event: ' + data.from);
});

ipcMain.on('newToken', (event, data) => {
    tokenManager.accessToken.set(data.accesstoken, 'Main process ipc event: ' + data.from);
});

app.on(events.showNotification, content => {
    switch (content.type) {
        case 'normal': {
            content = Object.assign({ isReplyable: false }, content);
            break;
        }

        case 'replyable': {
            content = Object.assign({ isReplyable: true }, content);
            break;
        }

        default: {
            content = Object.assign({ isReplyable: false }, content);
            break;
        }
    }

    let temp = {
        height: content.isReplyable ? 130 : 95,
        y: 50,
        x: screen.width - 400 - 20,
    };
    let notifOpts = Object.assign(temp, notificationOptions);

    if (Object.keys(notifs).length > 0) {
        queue.push({ notifOpts, content });
    } else {
        const window = createWindow('notification', notifOpts);
        notifs[window.id] = content;
    }
});

app.on(events.showMain, () => createMainWindow());

app.on('ready', () => {
    //TODO do not activate when production
    require('devtron').install();

    const {
        default: installExtension,
        REACT_DEVELOPER_TOOLS,
        REDUX_DEVTOOLS,
    } = require('electron-devtools-installer');

    installExtension([REDUX_DEVTOOLS, REACT_DEVELOPER_TOOLS])
        .then(name => {
            //eslint-disable-next-line
            console.log(`Added Extension:  ${name}`);
        })
        .catch(err => {
            //eslint-disable-next-line
            console.log("An error occurred: ", err);
        });

    screen = electron.screen.getPrimaryDisplay().workAreaSize;
    tray = require('./app/src/desktop/tray')();
    createMainWindow();
});

app.on('certificate-error', (event, webContent, url, error, certificate, cb) => {
    if (url.startsWith('https://localhost:4000/')) {
        event.preventDefault();
        return cb(true);
    }
    console.error('Certificate invalid: ' + url + ' ' + error);
    return cb(false);
});

app.on('window-all-closed', () => {
    //Nothing to do to avoid closing
});

app.on('exit', () => {
    if (socket.is(socket.status.connected)) socket.close();
});
