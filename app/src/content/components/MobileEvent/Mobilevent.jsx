import React, { Component } from 'react';
import styles from './styles.css';

export default class Mobilevent extends Component {
    constructor(props) {
        super(props);
        this.state = { tab: 'msg' };
    }

    render() {
        return (
            <div>
                <div
                    className={styles.container}
                    style={{
                        display: this.props.hide ? 'none' : 'block',
                    }}
                >
                    <div className={styles.tabs}>
                        <div
                            className={
                                this.state.tab !== 'msg'
                                    ? [styles.tab, styles.tab_selected].join(' ')
                                    : styles.tab
                            }
                            onClick={() => this.setState({ ...this.state, tab: 'msg' })}
                        >
                            <p>Messages</p>
                        </div>
                        <div
                            className={
                                this.state.tab !== 'call'
                                    ? [styles.tab, styles.tab_selected].join(' ')
                                    : styles.tab
                            }
                            onClick={() => this.setState({ ...this.state, tab: 'call' })}
                        >
                            <p>Calls</p>
                        </div>
                    </div>
                    <div className={styles.content}>
                        <div style={{ display: this.state.tab === 'msg' ? 'block' : 'none' }}>
                            {' '}
                            Messages content
                        </div>
                        <div style={{ display: this.state.tab === 'call' ? 'block' : 'none' }}>
                            {' '}
                            Calls content
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
