const { app, Menu, Tray, BrowserWindow } = require('electron');
const events = require('./events');

let tray;

const status = {
    error: __dirname + '/../content/assets/tray/error.png',
    ok: __dirname + '/../content/assets/tray/error.png',
    normal: __dirname + '/../content/assets/tray/normal.png',
};

const createTray = () => {
    tray = new Tray(status.normal);
    const contextMenu = Menu.buildFromTemplate([
        {
            label: 'Open sharette manager',
            click: function() {
                app.emit(events.showMain);
            },
        },
        {
            label: 'Exit',
            click: function() {
                app.quit();
            },
        },
    ]);
    tray.setContextMenu(contextMenu);
    tray.setToolTip('Sharette Notification');
    tray.on('click', () => {
        const windows = BrowserWindow.getAllWindows();
        if (windows.length !== 0) {
            const window = windows.filter(window => window.getTitle() === 'Sharette Manager');
            if (window.length < 0) {
                app.emit(events.showMain);
            } else {
                window[0].focus();
            }
        } else {
            app.emit(events.showMain);
        }
    });
    return {
        changeStatus,
        status,
    };
};

const changeStatus = status => {
    tray.setImage(status);
};

module.exports = createTray;
