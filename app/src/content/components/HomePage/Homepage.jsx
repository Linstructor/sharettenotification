import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreator from '../../../store/main/actionCreators';
import styles from './styles.css';

import jwt from 'jsonwebtoken';
import Mobilevent from '../MobileEvent/Mobilevent';
import SettingMenu from '../Settings/SettingMenu/SettingMenu';

class Homepage extends Component {
    constructor(props) {
        super(props);
        this.action = bindActionCreators(ActionCreator, props.dispatch);
        this.state = { settings: false };
    }

    render() {
        return (
            <div className={styles.full_page}>
                <div className={styles.header}>
                    <div className={styles.header_content}>
                        <div className={styles.flx}>
                            <p
                                className="material-icons"
                                style={{
                                    cursor: 'pointer',
                                    display: this.state.settings ? 'block' : 'none',
                                }}
                                onClick={() => this.setState({ ...this.state, settings: false })}
                            >
                                arrow_back
                            </p>
                            <p style={{ fontSize: '20px' }}>{this.props.store.email}</p>
                        </div>
                        <p
                            className={['material-icons', styles.header_settings].join(' ')}
                            onClick={() => this.setState({ ...this.state, settings: true })}
                        >
                            settings
                        </p>
                    </div>
                </div>
                <div className={styles.home_content}>
                    <Mobilevent hide={this.state.settings} />
                    <div style={{ display: this.state.settings ? 'block' : 'none' }}>
                        <SettingMenu />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const decode = jwt.decode(state.accesstoken);
    return {
        store: {
            email: decode !== null ? decode.email : 'none',
            accesstoken: state.accesstoken,
        },
    };
};

export default connect(mapStateToProps)(Homepage);
