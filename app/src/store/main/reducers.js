import Actions from './actions';
import Default from './default';
import { ipcRenderer } from 'electron';

/* eslint-disable indent */
export default (state = Default, action) => {
    switch (action.type) {
        case Actions.tokenSetter:
            if (action.payload.goMain) ipcRenderer.send('newToken', action.payload);
            return { ...state, accesstoken: action.payload.accesstoken };
        case Actions.refreshTokenSetter: {
            ipcRenderer.send('newRefreshToken', action.payload);
            return { ...state, refreshToken: action.payload.refreshToken };
        }
        case Actions.defaultSetter: {
            return {
                ...state,
                accesstoken: action.payload.accesstoken,
                refreshToken: action.payload.refreshToken,
            };
        }
        default:
            return state;
    }
};
