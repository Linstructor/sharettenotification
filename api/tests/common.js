import jwt from 'jsonwebtoken';

/**
 * Create a token for test
 * @param secret {String}
 * @param data {Object}
 * @param expiration? {Integer} default 10
 * @returns {Promise<*>}Error/token
 */
export const createFakeToken = async (secret, data = {}, expiration = 10) => {
    return new Promise((resolve, reject) => {
        jwt.sign(data, secret, { expiresIn: expiration }, (err, token) => {
            if (err) reject(err.message);
            resolve(token);
        });
    });
};
