require('babel-register');
import config from './config';

import { start } from './api';

start()
    .then(() => console.log('Everything started successfully'))
    .catch(() => console.log('Error during start'));
