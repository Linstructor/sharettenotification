import { ipcRenderer } from 'electron';

import React from 'react';
import { render } from 'react-dom';
import { bindActionCreators, createStore } from 'redux';
import { Provider } from 'react-redux';

import './global.css';
import reducers from '../../../store/main/reducers';
import { HashRouter, Route } from 'react-router-dom';
import Login from '../../components/Login/login';
import Loaderpage from '../../components/LoaderPage/loaderpage';
import Homepage from '../../components/HomePage/Homepage';
import Register from '../../components/Register/register';
import * as ActionCreator from "../../../store/main/actionCreators";

const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

const actions = bindActionCreators(ActionCreator, store.dispatch);

ipcRenderer.on('newTokenWClb', (_, token) => {
    actions.setTokenWithoutCallback(token, 'Send from electron');
});

ipcRenderer.on('newToken', (_, token) => {
    actions.setToken(token, 'Send from electron');
});

ipcRenderer.on('newRefreshToken', (_, token) => {
    actions.setRefreshToken(token, 'Send from electron');
});

ipcRenderer.on('content', (_, data) => {
    actions.setStartToken(data.accesstoken, data.refreshtoken);
});

render(
    <Provider store={store}>
        <HashRouter>
            <div>
                <Route path="/" exact component={Loaderpage} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/home" component={Homepage} />
            </div>
        </HashRouter>
    </Provider>,
    document.getElementById('app'),
);

if (module.hot) {
    module.hot.accept();
}
