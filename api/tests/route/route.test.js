/* eslint-disable no-undef,no-unused-vars */
import config from '../../config';

import * as Mongodb from '../../databases/mongodb';
import * as Redis from '../../databases/redis';
import Hash from '../../databases/Murmurhash';
import * as jwt from '../../middleware/jwt';

import { createFakeToken } from '../common';

import chakram from 'chakram';

const address = `https://localhost:${process.env.api_port}`;

let api;

beforeAll(async () => {
    const result = await chakram.get(address, { rejectUnauthorized: false });
    if (result.error) {
        console.warn('Starting a new Api');
        api = require('../../api');
        await api.start();
    }
    await Mongodb.connect();
});

afterAll(async () => {
    if (api) {
        console.warn('Temp api closing');
        await api.close();
    }
    await Mongodb.close();
});

describe('/user', () => {
    afterAll(async () => {
        await Mongodb.user.deleteWithInfo({ email: 'route.user@mail.com' });
    });

    test('Create user successfully', () => {
        return chakram
            .post(
                address + '/user',
                {
                    email: 'route.user@mail.com',
                    password: 'password',
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.response.statusCode).toBe(201);
            });
    });

    test('Modify password', () => {
        return Mongodb.user.getUserInformation({ email: 'route.user@mail.com' }).then(user => {
            return jwt.createToken(user.uid, 'route.user@mail.com').then(token => {
                return chakram
                    .patch(
                        address + '/user',
                        {
                            password: 'newpassword',
                        },
                        {
                            rejectUnauthorized: false,
                            headers: {
                                Authorization: `Bearer ${token}`,
                            },
                        },
                    )
                    .then(response => {
                        return chakram
                            .post(
                                address + '/login',
                                {
                                    email: 'route.user@mail.com',
                                    password: 'password',
                                },
                                { rejectUnauthorized: false },
                            )
                            .then(result => {
                                expect(response.response.statusCode).toBe(204);
                                return expect(result.response.statusCode).toBe(403);
                            });
                    });
            });
        });
    });
});

describe('/login', () => {
    jest.setTimeout(3000);
    beforeAll(async () => {
        await Mongodb.user
            .createUser('my.mail@mail.com', 'mypassword')
            .catch(err => console.error(err));
    });

    afterAll(async () => {
        await Mongodb.user.deleteWithInfo({ email: 'my.mail@mail.com' });
    });

    test('Success', async () => {
        return chakram
            .post(
                address + '/login',
                {
                    email: 'my.mail@mail.com',
                    password: 'mypassword',
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.error).toBeNull();
                expect(result.response.body.token).not.toBeNull();
            });
    });

    test('Fail partial invalid', () => {
        return chakram
            .post(
                address + '/login',
                {
                    email: 'my.mail@mail.com',
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.error).toBeNull();
                expect(result.response.statusCode).toBe(400);
            });
    });

    test('Fail bad password', () => {
        return chakram
            .post(
                address + '/login',
                {
                    email: 'my.mail@mail.com',
                    password: 'no',
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.error).toBeNull();
                expect(result.response.statusCode).toBe(403);
            });
    });
});

describe('/token', () => {
    let hash, token;
    beforeAll(async () => {
        await Redis.connect();
        token = await createFakeToken(process.env.jwt_secret, { uid: 42 });
        await Redis.token.addRefreshToken(token, 5);
        hash = Hash(JSON.stringify({ token }), 42);
    });

    afterAll(async () => {
        await Redis.close();
    });

    test('Refresh success', () => {
        return chakram
            .post(
                address + '/token?refresh=true',
                {
                    accesstoken: token,
                    refreshtoken: hash,
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.error).toBeNull();
                expect(result.response.statusCode).toBe(201);
                expect(result.response.body.accesstoken).not.toBeNull();
            });
    });

    test('Refresh fail partial invalid', () => {
        return chakram
            .post(
                address + '/token?refresh=true',
                {
                    accesstoken: token,
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.error).toBeNull();
                expect(result.response.statusCode).toBe(400);
            });
    });

    test('Refresh fail bad refresh token', () => {
        return chakram
            .post(
                address + '/token?refresh=true',
                {
                    accesstoken: token,
                    refreshtoken: hash + 'f',
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.error).toBeNull();
                expect(result.response.statusCode).toBe(410);
            });
    });

    test('Refresh fail bad token', () => {
        return chakram
            .post(
                address + '/token?refresh=true',
                {
                    accesstoken: token + 'g',
                    refreshtoken: hash,
                },
                { rejectUnauthorized: false },
            )
            .then(result => {
                expect(result.error).toBeNull();
                expect(result.response.statusCode).toBe(410);
            });
    });
});

test('Token expiration', async () => {
    const token = await createFakeToken(
        process.env.jwt_secret,
        {
            content: 'none',
        },
        0,
    );
    return chakram
        .get(address + '/auth', {
            rejectUnauthorized: false,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then(result => {
            expect(result.error).toBeNull();
            expect(result.response.statusCode).toBe(410);
        });
});
