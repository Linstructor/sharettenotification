import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreator from '../../../store/main/actionCreators';
import Progressbar from '../ProgressBar/Progressbar';
import { ipcRenderer } from 'electron';

const regexMail = /^[\w\d]+((\.[\w\d]+)?)+@[\w\d]+\.([\w\d]+)((\.[\w\d]+)?)+$/;
const regexPass = /^[\w\d!$=+()?/]{6,}$/;

console.log('/LoaderPage');

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { email: '', password: '', loginin: false, wrong: false };
        this.login = this.login.bind(this);
        this.enterPressed = this.enterPressed.bind(this);
        this.action = bindActionCreators(ActionCreator, props.dispatch);
        this.loginButton = React.createRef();
    }

    handleChange(name, event) {
        const checkString = regex => {
            return regex.test(event.target.value) ? '#42b983' : 'red';
        };
        event.target.style.borderBottomColor =
            name === 'email' ? checkString(regexMail) : checkString(regexPass);
        this.setState({ ...this.state, [name]: event.target.value.toString().trim() });
    }

    login() {
        if (this.state.loginin) return;
        this.setState({ ...this.state, wrong: false });
        if (
            this.state.email === '' ||
            this.state.password === '' ||
            !regexMail.test(this.state.email)
        ) {
            return this.setState({ ...this.state, wrong: true });
        }
        this.setState({ ...this.state, loginin: !this.state.loginin });

        fetch('https://localhost:4000/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
            }),
        })
            .then(result => {
                if (result.status === 403 || result.status === 400) {
                    console.log('Login failed');
                    this.setState({ ...this.state, wrong: true });
                } else {
                    console.log('Login successfully');
                    result.json().then(body => {
                        this.action.setToken(body.accesstoken, 'login');
                        this.action.setRefreshToken(body.refreshtoken, 'login');
                        ipcRenderer.send('startSocket');
                        this.props.history.push('/home');
                    });
                }
                this.setState({ ...this.state, loginin: !this.state.loginin });
            })
            .catch(err => {
                this.setState({ ...this.state, loginin: !this.state.loginin });
                console.log('error during fetch ' + err);
            });
    }

    enterPressed(event) {
        if (event.key === 'Enter') {
            if (this.state.password !== '' && this.state.email !== '')
                this.loginButton.current.click();
        }
    }

    render() {
        return (
            <div onKeyPress={this.enterPressed}>
                <div className={styles.input_container}>
                    <p className="material-icons">account_circle</p>
                    <input
                        className={styles.input}
                        placeholder="Email"
                        value={this.state.email}
                        onChange={this.handleChange.bind(this, 'email')}
                        type="email"
                    />
                </div>

                <div className={styles.input_container}>
                    <p className="material-icons">lock</p>
                    <input
                        className={styles.input}
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleChange.bind(this, 'password')}
                        type="password"
                    />
                </div>

                <div ref={this.loginButton} className={styles.login_button} onClick={this.login}>
                    {this.state.loginin ? null : <p>Log in</p>}
                    {this.state.loginin ? <Progressbar infinite round size={25} /> : null}
                </div>

                {this.state.wrong ? (
                    <p className={styles.invalid}>Invalid email or password</p>
                ) : null}
                <Link to="/register">
                    <p className={styles.register_link}>Register now</p>
                </Link>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        store: {
            accesstoken: state.accesstoken,
            refreshToken: state.refreshToken,
        },
    };
};

export default connect(mapStateToProps)(Login);
