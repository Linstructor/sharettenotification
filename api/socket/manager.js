import { fork } from 'child_process';

let socketServer;

export const open = () => {
    socketServer = fork('./api/socket/server.js');

    socketServer.on('error', err => {
        console.error('Websocket fork err: ' + err);
    });
};

export const close = () => {
    return new Promise((resolve, reject) => {
        socketServer.on('message', msg => {
            if (msg.state === 'closed') console.log('WebsocketServer closed');
        });
        socketServer.on('exit', (code, signal) => {
            resolve({ code, signal });
        });
        socketServer.on('error', err => {
            reject(err);
        });
        socketServer.send({ action: 'close' });
    });
    //TODO close socket and server
};
