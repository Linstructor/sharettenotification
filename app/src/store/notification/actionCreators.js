import Actions from './actions';

export const initNotification = informations => {
    return {
        type: Actions.initNotification,
        payload: informations,
    };
};
