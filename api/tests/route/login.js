import supertest from 'supertest';

const request = supertest(`http://localhost:${process.env.api_port}`);

export const PostLogin = () => {
    return new Promise((resolve, reject) => {
        try {
            request()
                .post('/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'ABCD',
                    password: 'PASSWORD',
                    plateform: 'Desktop',
                })
                .expect(200)
                .end((err, res) => {
                    if (err) reject(err.message + ':\t' + res.error.text);
                    resolve('Ok');
                });
        } catch (err) {
            reject(err);
        }
    });
};

export const PostLoginFailed = () => {
    return new Promise((resolve, reject) => {
        try {
            request()
                .post('/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'ABCD',
                })
                .expect(401)
                .end((err, res) => {
                    if (err) reject(err.message + ':\t' + res.error.text);
                    resolve('Ok');
                });
        } catch (err) {
            reject(err);
        }
    });
};
