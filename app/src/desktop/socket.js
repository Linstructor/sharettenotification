const { app } = require('electron');

const WebSocket = require('ws');
const { isNull, isUndefined } = require('util');
const events = require('./events');

const status = { none: 'none', connected: 'connected', disconnected: 'disconnected' };

let ws,
    manager,
    currentState = status.none;

const connect = (ip = '127.0.0.1', port = 4001, tokenManager) => {
    if (typeof ip === 'object') {
        tokenManager = ip;
        ip = '127.0.0.1';
    }

    return new Promise((resolve, reject) => {
        if (isNull(tokenManager) || isUndefined(tokenManager)) throw 'Token manager is incorect';
        ws = new WebSocket(`wss://${ip}:${port}/`, 'sharette-protocol', {
            rejectUnauthorized: false,
            handshakeTimeout: 5000,
            headers: {
                authorization: 'Desktop ' + tokenManager.accessToken.get(),
            },
        });

        ws.once('open', function open() {
            manager = tokenManager;
            currentState = status.connected;
            resolve();
        });

        ws.once('error', err => {
            currentState = status.disconnected;
            reject(err);
        });

        ws.once('unexpected-response', req => {
            ws.terminate();
            reject(req.res.statusCode);
        });
    });
};

const listen = () => {
    console.log('Listening');
    ws.on('close', (code, msg) => {
        console.log('close ' + code + ' ' + msg);
        currentState = status.disconnected;
    });

    ws.on('error', err => {
        // console.log(err);
        currentState = status.disconnected;
    });

    ws.on('message', msg => {
        msg = JSON.parse(msg);

        if (msg.action === 'notification') {
            console.log('new notification');
            app.emit(events.showNotification, msg.data);
        }

        if (msg.action === 'newToken') {
            console.log('New token');
            manager.accessToken.set(msg.data.accesstoken, 'Socket');
        }

        if (msg.action === 'paper') {
            // console.log('Paper asked');
            ws.send(
                JSON.stringify({
                    action: 'verify',
                    data: { accesstoken: manager.accessToken.get() },
                }),
            );
        }

        if (msg.action === 'expire') {
            // console.log('Expired');
            ws.send(
                JSON.stringify({
                    action: 'refresh',
                    data: {
                        accesstoken: manager.accessToken.get(),
                        refreshtoken: manager.refreshToken.get(),
                    },
                }),
            );
        }
    });
};

const respondNotif = msg => {
    ws.send(JSON.stringify({ action: 'response', data: msg }));
};

const close = () => {
    console.log('Close');
    if (isNull(ws) || isUndefined(ws)) throw 'The socket is not connected';
    ws.terminate();
};

const is = state => {
    return state === currentState;
};

module.exports = {
    connect,
    listen,
    respondNotif,
    close,
    is,
    status,
};
