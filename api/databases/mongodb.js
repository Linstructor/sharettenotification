import mongoose from 'mongoose';
import uuidv4 from 'uuid/v4';
import bcrypt from 'bcrypt';
import { isUndefined, isNull } from 'util';

import Model from '../model/mongo/model';

const error = {
    infoMissing: 'No info to find the user',
    emailUsed: 'Email already used',
    noUser: 'No user',
};

const options = {
    useNewUrlParser: true,
    keepAlive: true,
    keepAliveInitialDelay: 300000,
    autoReconnect: true,
    autoIndex: false,
    reconnectTries: 3,
    reconnectInterval: 3000,
    connectTimeoutMS: 10000,
    auth: {
        user: process.env.mongo_auth,
        password: process.env.mongo_pass,
        authdb: 'admin',
    },
};

/**
 * Create a hash with a password
 * @param password
 * @returns {Promise<any>} Error/hash
 */
const createHashPass = password => {
    return new Promise((resolve, reject) => {
        bcrypt
            .genSalt(parseInt(process.env.bcrypt_saltround))
            .then(salt => {
                bcrypt
                    .hash(password, salt)
                    .then(hash => resolve(hash))
                    .catch(err => reject(err));
            })
            .catch(err => reject(err));
    });
};

/**
 * Connect to a Mongodb
 * @param ip {String}Database ip - default 127.0.0.1
 * @param database {String}Database name - default 'sharette'
 * @returns {Promise<any>}Error/null
 */
export const connect = (ip = '127.0.0.1', database = 'sharette') => {
    return new Promise((resolve, reject) => {
        mongoose
            .connect(
                `mongodb://${ip}:${process.env.mongo_port}/${database}`,
                options,
            )
            .then(() => resolve())
            .catch(err => reject(err));
    });
};

/**
 * Create user inside mongo
 * @param email {String}
 * @param password {String}
 * @returns {Promise<any>}Error/uid
 */
const createUser = (email, password) => {
    return new Promise((resolve, reject) => {
        if (isUndefined(email) || isUndefined(password)) reject(error.infoMissing);
        getUserInformation({ email })
            .then(() => reject(error.emailUsed))
            .catch(() => {
                createHashPass(password)
                    .then(hash => {
                        const newUser = new Model.User({
                            email: email,
                            password: hash,
                            uid: uuidv4(),
                        });
                        newUser.save((err, user) => {
                            if (err) return reject(err);
                            resolve(user.uid);
                        });
                    })
                    .catch(err => reject(err));
            });
    });
};

/**
 * Verify log in information
 * @param email {String}
 * @param password {String}
 * @returns {Promise<any>}Error/uid
 */
const verifyUserCredentials = (email, password) => {
    return new Promise((resolve, reject) => {
        getUserInformation({ email })
            .then(user => {
                bcrypt
                    .compare(password, user.password)
                    .then(result => {
                        result ? resolve(user.uid) : reject('Denied');
                    })
                    .catch(err => reject(err));
            })
            .catch(err => reject(err));
    });
};

/**
 * Create filter to determine if mongo need to search with email or uid
 * @param info {Object}Object containing email or uid field
 */
const createUserFilter = info => {
    if (isUndefined(info.email) && isUndefined(info.uid)) throw error.infoMissing;
    let filter = {};
    if (!isUndefined(info.email)) filter['email'] = info.email;
    if (!isUndefined(info.uid)) filter['uid'] = info.uid;
    return filter;
};

/**
 * Get user information stored mongo
 * @param info {Object}Object containing email or uid field
 * @returns {Promise<any>}Error/(user or null)
 */
const getUserInformation = info => {
    return new Promise((resolve, reject) => {
        let filter;
        try {
            filter = createUserFilter(info);
        } catch (e) {
            return reject(e);
        }
        Model.User.where(filter).findOne(filter, (err, user) => {
            if (err) return reject(err);
            isNull(user) ? reject(error.noUser) : resolve(user);
        });
    });
};

/**
 * Delete user in mongodb
 * @param info {Object}Object containing email or uid field
 * @returns {Promise<any>}Error/'Deleted'
 */
const deleteWithInfo = info => {
    return new Promise((resolve, reject) => {
        let filter;
        try {
            filter = createUserFilter(info);
        } catch (e) {
            return reject(e);
        }
        Model.User.where(filter).findOneAndDelete(filter, err => {
            if (err) return reject(err);
            resolve('Deleted');
        });
    });
};

const modifyPassword = (uid, password) => {
    return new Promise((resolve, reject) => {
        if (isUndefined(uid) || isNull(uid)) return reject('Uid null');

        createHashPass(password)
            .then(hash => {
                Model.User.updateOne({ uid }, { password: hash }, (err, user) => {
                    if (err) return reject(err);
                    if (user.ok === 0 || user.nModified === 0) return reject('Nothing modified');
                    resolve();
                });
            })
            .catch(err => reject(err));
    });
};

/**
 * Close mongodb connection
 * @returns {Promise<any>}Error/null
 */
export const close = () => {
    return new Promise(resolve => {
        mongoose
            .disconnect()
            .then(() => resolve())
            .catch(err => resolve(err));
    });
};

export const user = {
    createUser,
    verifyUserCredentials,
    getUserInformation,
    deleteWithInfo,
    modifyPassword,
};
